<?php

include 'conf.local.php';
include '_func.php';

$file = 'BGA-export.csv';
$myname = 'Noizette';
$myusername = 'Noizette';
$imported = 0;
$data = [];

// Craft your own!
// Game id is in BGG URL: https://boardgamegeek.com/boardgame/idxxx/game-name
$games = [
    '2719' => 'Puissance Quatre',
    '68448' => '7 Wonders',
    '264055' => 'Draftosaurus',
    '822' => 'Carcassonne',
    '341358' => 'Insert',
    '2389' => 'Reversi',
    '432' => '6 qui prend !',
    '105265' => 'Battle of LITS',
    '11929' => 'Gomoku',
    '526' => 'Abalone',
    '204583' => 'Kingdomino',
    '344017' => 'Dodo',
    '320505' => 'Mattock',
    '2083' => 'Dames',
    '255027' => 'Quinque',
    '25821' => 'Les Loups-Garous de Thiercelieux',
    '3319' => 'Neutreeko',
    '2397' => 'Backgammon',
    '191925' => 'Bandido',
    '188' => 'Go',
    '140535' => 'Koryŏ',
    '624' => 'Quoridor',
    '4112' => 'Hex',
    '2386' => 'Dames chinoises',
    '173346' => '7 Wonders Duel',
    '9220' => 'Saboteur',
    '171' => 'Échecs',  
    ];

if (($handle = fopen($file, 'r')) !== false)
{
    while (($csv = fgetcsv($handle, 0, ';', '"')) !== false)
    {
        // Parse CSV
        if (($id = array_search(trim($csv[1]), $games)) == false)
        {
            echo $csv[1]. ' is not in our list, ignoring.';
            continue;
        }

        unset($data);
        $data = [
            'ajax' => "1",
            'action' => "save",
            'version' => "2",
            'dateinput' => date('Y-m-d'),
            'quantity' => "1",
            'length' => "",
            'incomplete' => "0",
            'nowinstats' => "0",
            'twitter' => "0",
            'location' => "BGA",
            'objectid' => "68448",
            'objecttype' => "thing",
            'comments' => "Imported by @Noizette script - https://gitlab.com/ramoloss/bgg-import-plays-from-csv"];

        // Game info
        $data['objectid'] = $id;
        $data['playdate'] = DateTime::createFromFormat('d/m/Y', $csv[2])->format('Y-m-d');
        $data['length'] = $csv[3];

        // Players info
        // Assuming format '! 1ᵉʳ# Player name 1 #54 ?! 2ᵉ# Player2 #23 ?! 3ᵉ# Player naaammmee #19 ?'
        $players = explode('?', $csv[0]);
        array_pop($players);
        foreach ($players as $i => $str)
        {
            $name = preg_replace('/^! \d+(ᵉʳ|ᵉ)# (.*) #(-{0,1}\d+) /', '$2', $str);
            $data['players['.$i.'][name]'] = $name;
            $data['players['.$i.'][username]'] = $name == $myname ? $myusername:'';
            $data['players['.$i.'][color]'] = "";
            $data['players['.$i.'][position]'] = "";
            $data['players['.$i.'][score]'] = preg_replace('/^! \d+(ᵉʳ|ᵉ)# .* #(-{0,1}\d+) /', '$2', $str);
            $data['players['.$i.'][rating]'] = "";
            $data['players['.$i.'][win]'] = (int) preg_replace('/^! (\d+)(ᵉʳ|ᵉ)#/', '$1', $str) == 1 ? 1:0;
            $data['players['.$i.'][new]'] = "0";
        }

        // Prepare request
        $content = '';
        foreach ($data as $k => $v)
        {
            $content .= $k . '=' . urlencode($v) . '&';
        }
        // Remove last ampersand
        $content = substr($content, 0, -1);

        // curl request
        if (curl($content) === false) {
            echo "Woops, ". $csv[1]." - ". $csv[2]." failed!\n";
        }
        
        $imported++;
        echo $csv[1] ." - ". $csv[2] ." imported. Total $imported \n";
    }

}

echo "Done!\n";