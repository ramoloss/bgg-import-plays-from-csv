<?php

function curl($content)
{
    // Preparing cURL session
    $crl = curl_init(URL);
    curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($crl, CURLINFO_HEADER_OUT, true);
    curl_setopt($crl, CURLOPT_POST, true);
    curl_setopt($crl, CURLOPT_POSTFIELDS, $content);
    
    // There probably are some useless headers, I just copied them all from firefox when using SPLU
    // Put your actual cookies for authentication
    curl_setopt($crl, CURLOPT_HTTPHEADER, array(
        'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0',
        'Accept: application/json, text/plain, */*',
        'Accept-Language: en,fr;q=0.8,fr-FR;q=0.5,en-US;q=0.3',
        'Accept-Encoding: gzip, deflate, br',
        'Referer: https://boardgamegeek.com/',
        'Content-type: application/x-www-form-urlencoded',
        'Content-Length: '.strlen($content),
        'Origin: https://boardgamegeek.com',
        'Connection: keep-alive',
        COOKIE,
        'Sec-Fetch-Dest: empty',
        'Sec-Fetch-Mode: cors',
        'Sec-Fetch-Site: same-origin',
        'Pragma: no-cache',
        'Cache-Control: no-cache',
        'TE: trailers'
        ));
    
    // Submit the POST request
    $result = curl_exec($crl);

    // Close cURL session handle
    curl_close($crl);

    return $result;
}


function deletePlay(int $playId)
{
    $data = [
        "finalize" => "1",
        "action" => "delete",
        "playid" => $playId
    ];
    
    // Prepare request
    $content = '';
    foreach ($data as $k => $v)
    {
        $content .= $k . '=' . urlencode($v) . '&';
    }
    // Remove last ampersand
    $content = substr($content, 0, -1);
    
    // Handle cURL error
    if (curl($content) === false) {
        echo "Woops, $playId failed!\n";
        return false;
    }
    
    return true;
}