<?php

include 'conf.local.php';
include '_func.php';

$file = 'plays.json';

// Those JSON files are get via SPLU (backup function)
// or via the dev console, network tab, when selecting "Get all" from plays history

/*
$json = file_get_contents($file);
$plays = json_decode($json, true)['plays'];
/*/
$json = file_get_contents($file);
$plays = json_decode($json, true);
//*/

$total = $count = 0;
foreach ($plays as $k => $play)
{
    if (!is_array($play))
    {
        continue;
    }
    if ($play['location'] == 'BGA')
    {
        if (deletePlay($play['playid']) == false)
        {
            break;
        }
        echo $play['playid']. "(".$play['name'].", ".$play['playdate'].")deleted.\n";
        $count++;
    }
    $total++;
}

echo $count." plays deleted.\n".$total." plays fetched in history.\n";